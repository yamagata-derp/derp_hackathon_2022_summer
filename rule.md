
{% hint style='info' %}
インストールするパッケージに関する情報
{% endhint %}

{% hint style='tip' %}
それ以外の情報と例外情報
{% endhint %}

{% hint style='danger' %}
読者にとって読み飛ばすと致命的な問題
{% endhint %}

{% hint style='working' %}
必要に応じて行う必要がある操作の記載
{% endhint %}
