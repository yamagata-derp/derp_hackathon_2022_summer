# Reactnativeとは
　これから用いるReactnativeとはなんなのか、Reactnativeの何がよく通常のネイティブアプリの開発（SwiftやJava/Kotlin）とは何が違うのか簡単にご紹介します。

## ネイティブアプリとクロスプラットフォームフレームワーク
　通常 iOSやAndroidなとのスマートフォンのアプリケーションはネイティブアプリと言います。それらのネイティブアプリはSwiftやJava(Kotlin)で作成します。

　しかし、SwiftやJava(Kotlin)以外の言語でモバイルアプリを作れる、いわゆるクロスプラットフォームフレームワークと呼ばれるものがあります。Reactnativeはそのクロスプラットフォームフレームワークの一つです。

　クロスプラットフォームフレームワークにはFacebookが開発を主導しているReactnative以外にもGoogleが開発を主導しているFlutterやMicrosoftが開発を主導しているXamarinなどがあります。

# Reactnativeのメリット

　Reactnativeには大きく分けて

- ネイティブアプリの体験を実現できる点
- Learn once, Write anywhere思想に基づく開発
- コードの共通化ができる
- コンパイルが不要


### ネイティブアプリの体験を実現できる点

　ReactnativeはネイティブAPIを使ってUIを描写しているのでネイティブアプリ同様の体験をアプリに組み込むことができます。

### Learn once, Write anywhere思想に基づく開発

　一度学べばどのプラットフォームも使えるという思想です。

### コードの共通化ができる

　ReactnativeはSwiftやJava(Kotlin)と別々に開発する必要がなくコードの再利用が可能です。

### コンパイルが不要

　通常SwiftやJava(Kotlin)を用いた開発にはコンパイルが必要でコードを修正するなど細かいことに意外に時間がかかったりしますがReactnativeではそのコンパイルがないため効率の良い開発ができます。

### 注意

　Reactnativeをはじめとするクロスプラットフォームフレームワークは完璧というわけではなく個別にネイティブアプリで実装した方がいい場合があります。